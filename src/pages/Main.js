import React from "react";
import Person from "../components/Main/person";
import Container from '@material-ui/core/Container';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    }
  }

  componentDidMount() {
    const url = "https://reqres.in/api/users?page=2";
    fetch(url)
      .then(res => res.json())
      .then(el => this.setState({ data: el.data }));
  }

  render() {
    const { data } = this.state;
    return (
      <Container fixed>
        <div className="main-block">
          {data.map(item => (
            <Person
              key={item.id}
              avatar={item.avatar}
              email={item.email}
              firstname={item.first_name}
              lastname={item.last_name}
            />
          ))}
        </div>
      </Container>

    );
  }
}

export default Main;
