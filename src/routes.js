import Main from "./pages/Main";
import Auth from "./pages/Auth";
import Profile from "./pages/Profile";

export const routes = [
    {
        isNavBar: true,
        isExact: true,
        path: "/",
        component: Main,
        name: "Главная"
    },
    {
        isNavBar: true,
        path: "/profile",
        component: Profile,
        name: "Профиль",
        isPrivate: true
    },
    {
        path: "/auth",
        component: Auth,
        name: "Авторизация"
    }
]