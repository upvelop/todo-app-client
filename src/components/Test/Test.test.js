import React from 'react';
import renderer from 'react-test-renderer';
import Test from './index';

it('Test Component match snapshot', () => {
  const component = renderer.create(
    <Test />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
