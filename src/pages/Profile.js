import React from 'react';
import { connect } from 'react-redux';
import { SIGN_OUT } from '../store/action';
import { Button } from '@material-ui/core';


class Profile extends React.Component {

  render() {
    return (
      <div>
        <div className="profile">
          <h2>Профиль</h2>
          <div className="profile-info">
            <div className="profile-info-name">
              Ваше имя: {this.props.login}
            </div>
            <Button
              onClick={this.signOut}
              variant="contained"
              color="primary"
            >
              Выйти
            </Button>
          </div>
        </div>
      </div>
    );
  }

  signOut = () => {
    this.props.signOut();
  };
}

const mapStateToProps = (state) => (
  {
    login: state.login
  }
);

const mapDispatchToProps = (dispatch) => (
  {
    signOut: () => dispatch({ type: SIGN_OUT })
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
