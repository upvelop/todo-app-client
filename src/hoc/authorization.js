import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const Athorization = (WrappedComponent) => {
  class WithAuthorization extends React.Component {

    render() {
      const { isAuthorized } = this.props;
      if (!isAuthorized) {
        return <Redirect to='/auth' />;
      }
      return <WrappedComponent {...this.props} />;
    }
  }

  const mapStateToProps = (state) => (
    {
      isAuthorized: Boolean(state.login)
    }
  );

  return connect(mapStateToProps)(WithAuthorization);
};

export default Athorization;
