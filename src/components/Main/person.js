import React from "react";

export default function Person(props) {
    return (
        <div className="person">
            <div className="person-avatar">
                <img src={props.avatar} alt={props.firstname} />
            </div>
            <div className="person-info">
                <div className="person-info__name">
                    {props.firstname}
                    {props.lastname}
                </div>
                <div className="person-info__email">
                    Email: {props.email}
                </div>
            </div>
        </div>
    );
}