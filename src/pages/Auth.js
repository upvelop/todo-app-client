import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import Paper from '@material-ui/core/Paper';
import Fade from '@material-ui/core/Fade';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "admin",
      password: "123",
      checked: false
    };
  }

  setLogin = event => {
    this.setState({ login: event.target.value });
  };

  setPassword = event => {
    this.setState({ password: event.target.value });
  };

  submitHandler = event => {
    event.preventDefault();
    const { login, password } = this.state;

    this.props.logIn(login, password);
  };

  handleChange = () => {
    this.setState({ checked: !this.state.checked })
  };

  render() {
    const { isAuthorized } = this.props;
    if (isAuthorized) {
      return <Redirect to="/" />;
    }
    const { login, checked, password } = this.state;
    const { error } = this.props;
    return (
      <div className="auth">
        <h1>Авторизация</h1>
        <form className="auth-block">
          <TextField
            className="auth-input"
            id="standard-basic-1"
            label="Логин"
            value={login}
            onChange={this.setLogin}
          />
          <TextField
            className="auth-input"
            id="standard-basic-2"
            label="Пароль"
            type="password"
            value={password}
            onChange={this.setPassword}
          />
          <Button
            className="auth-button"
            variant="contained"
            color="primary"
            onClick={this.submitHandler}
          >
            Войти
          </Button>
          <div className="error-message" hidden={!error}>
            {error}
          </div>
        </form>
        <FormControlLabel
          control={<Switch checked={checked} onChange={this.handleChange} color="primary" />}
          label="Подсказка"
        />
        <div className="">
          <Fade in={checked}>
            <Paper elevation={4} className="auth-hint">
              Логин: admin Пароль: 123
            </Paper>
          </Fade>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthorized: Boolean(state.login),
  error: state.errorMessage
});

const mapDispatchToProps = dispatch => ({
  logIn: (login, password) =>
    dispatch({ type: "LOG_IN", payload: { login, password } })
});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
