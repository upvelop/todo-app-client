import React from "react";
import "./App.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes } from "./routes";
import Menu from "./components/Menu/Menu";
import Authorization from "./hoc/authorization";

const renderSwitch = () => (
  <Switch>
    {routes.map(route => {
      const component = route.isPrivate ? Authorization(route.component) : route.component;
      return (
        <Route
          key={route.path}
          exact={route.isExact}
          path={route.path}
          component={component}
        />
      );
    })}
  </Switch>
);

class App extends React.Component {
  render() {

    return (
      <Router>
        <React.Fragment>
          <Menu routes={routes.filter(route => route.isNavBar)} />
          {renderSwitch()}
        </React.Fragment>
      </Router>
    );
  }
};

export default App;
